# README #

This repository just contains a demo of how one could test a very basic REST API
using C#/NUNIT. Some basic test cases are provided in a PDF. Not all of them are
implemented though. For the most part I've just given a sampling of test implementations.