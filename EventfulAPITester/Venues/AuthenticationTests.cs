﻿using NUnit.Framework;
using RestSharp;
using System.Xml;


namespace EventfulAPITester.Venues
{
    [TestFixture]
    public class AuthenticationTests
    {
        string VALID_APPKEY;
        //string VALID_USERNAME;
        //string VALID_PASSWORD;
        string INVALID_APPKEY;
        string BASE_URL;
        string VENUE_ID;

        XmlDocument ValidAppKeyAnonymousUser;
        XmlDocument InvalidAppKeyAnonymousUser;

        [OneTimeSetUp]
        public void PrepareBeforeAnyTests()
        {
            VALID_APPKEY = EventfulAPITester.Properties.Settings.Default.VALID_APP_KEY;
            INVALID_APPKEY = VALID_APPKEY + "foo";
            BASE_URL = @"http://api.eventful.com/rest/venues/get";
            VENUE_ID = "V0-001-007704945-3"; // This is the Rickshaw, one of Vancouver's finer venues.

            // Do the actual Requests
            ValidAppKeyAnonymousUser = GetResponseFromBasicRequest(VALID_APPKEY);
            InvalidAppKeyAnonymousUser = GetResponseFromBasicRequest(INVALID_APPKEY);
        }

        #region Valid App_Key with no User or Password provided
        [Test]
        public void Venues_Authentication_VerifyThat_Valid_AppKey_NoOAuth_Produces_NoError()
        {
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(ValidAppKeyAnonymousUser), Is.False);            
        }
        #endregion Valid App_Key with no User or Password provided

        #region Invalid App_Key with no User or Password provided
        [Test]
        public void Venues_Authentication_VerifyThat_Invalid_AppKey_NoOAuth_Produces_Error()
        {
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(InvalidAppKeyAnonymousUser), Is.True);           
        }

        [Test]
        public void Venues_Authentication_VerifyThat_Invalid_AppKey_NoOAuth_Produces_ErrorString()
        {   
            Assert.That(ResponseUtilities.GetErrorString(InvalidAppKeyAnonymousUser), Is.EqualTo("Authentication error"));
        }

        [Test]
        public void Venues_Authentication_VerifyThat_Invalid_AppKey_NoOAuth_Produces_ErrorDescription()
        {
            Assert.That(ResponseUtilities.GetErrorDescription(InvalidAppKeyAnonymousUser).Contains("A valid application key is required"), Is.True);
        }
        #endregion Invalid App_Key with no User or Password provided

        // It would be really nice to have some tests that checked
        // the behaviour when using Good and Bad oAUTH authentication.
        // This particular REST API doesn't require oAUTH authentication though.
        // FUTURE RESEARCH.
 
        public XmlDocument GetResponseFromBasicRequest(string app_key)
        {
            RestRequest request = new RestRequest(); // { Method = Method.GET };
            request.AddParameter("app_key", app_key);
            request.AddParameter("id", VENUE_ID);

            XmlDocument responseXml = ResponseUtilities.GetResponseContent(BASE_URL, app_key, request);
            return responseXml;
        }
    }
}
