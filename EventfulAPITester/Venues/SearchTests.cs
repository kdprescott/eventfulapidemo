﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Xml;
using RestSharp;

namespace EventfulAPITester.Venues
{
    [TestFixture]
    public class SearchTests
    {
        string VALID_APPKEY;
        //string VALID_USERNAME;
        //string VALID_PASSWORD;
        string INVALID_APPKEY;
        string BASE_URL;
        string VENUE_ID;

        [OneTimeSetUp]
        public void PrepareBeforeAnyTests()
        {
            VALID_APPKEY = EventfulAPITester.Properties.Settings.Default.VALID_APP_KEY;
            BASE_URL = @"http://api.eventful.com/rest/venues/get";
            VENUE_ID = "V0-001-007704945-3"; // This is the Rickshaw, one of Vancouver's finer venues.
        }

        [Test]
        public void Venues_Search_ByID_Successfully_Finds_Venue()
        {
            XmlDocument responseContent = GetResponseForSearchRequest(VENUE_ID);
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(responseContent), Is.False);
            Assert.That(ResponseUtilities.GetTagValue(responseContent, "//venue/name"), Is.EqualTo("The Rickshaw Theatre"));
            Assert.That(ResponseUtilities.GetTagValue(responseContent, "//venue/city"), Is.EqualTo("Vancouver"));
        }

        [Test]
        public void Venues_Search_ByID_Error_If_Venue_NOTFOUND()
        {
            XmlDocument responseContent = GetResponseForSearchRequest("V0-XYZ-1234567891-0"); // This is just a made up id
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(responseContent), Is.True);
            Assert.That(ResponseUtilities.GetErrorString(responseContent), Is.EqualTo("Not found"));
            Assert.That(ResponseUtilities.GetErrorDescription(responseContent), Is.EqualTo("There is no venue with that identifier."));
        }

        public XmlDocument GetResponseForSearchRequest(string venueId)
        {
            RestRequest request = new RestRequest(); // { Method = Method.GET };
            request.AddParameter("app_key", VALID_APPKEY);
            request.AddParameter("id", venueId);

            XmlDocument responseXml = ResponseUtilities.GetResponseContent(BASE_URL, VALID_APPKEY, request);
            return responseXml;
        }
    }
}
