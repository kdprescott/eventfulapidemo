﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RestSharp;
using System.Xml;

namespace EventfulAPITester.Venues
{
    [TestFixture]
    public class StructureTests
    {

        // *************************************************************
        // NOTE: I haven't implemented the exact tests set ou in the test plan.
        // Why?
        // 1. The test plan isn't exhaustive and is meant to be illustrative of what could be,
        //    and how to structure the test plan.
        // 2. I want to demonstrate a few different types of data in the actual test code.
        // 3. I also wanted to demonstrate the use of Test Case Factories in the code.

        string VALID_APPKEY;
        string BASE_URL;
        string VENUE_ID;

        XmlDocument validRequestDoc;

        [OneTimeSetUp]
        public void PrepareBeforeAnyTests()
        {
            VALID_APPKEY = EventfulAPITester.Properties.Settings.Default.VALID_APP_KEY;
            BASE_URL = @"http://api.eventful.com/rest/venues/get";
            VENUE_ID = "V0-001-007704945-3"; // This is the Rickshaw, one of Vancouver's finer venues.

            // Do the actual Requests
            validRequestDoc = GetResponseFromBasicRequest(VALID_APPKEY);
        }

        #region Tags Present
        [Test, TestCaseSource("TagExistCases")]
        public void Venues_Structure_Verify_Tag_Is_Present(string name)
        {
            Assert.That(ResponseUtilities.IsTagInXML(validRequestDoc, name), Is.True);
        }
        #endregion Tags Present

        #region Tag Value Data Types
        [Test, TestCaseSource("TagDataTypeCases")]
        public void Venues_Structure_Tag_Value_DataTypes(string name, string dataType)
        {
            string val = ResponseUtilities.GetTagValue(validRequestDoc, name);
            Assert.That(IsValueExpectedDataType(val, dataType), Is.True);
        }
        #endregion Tag Value Data Types


        public XmlDocument GetResponseFromBasicRequest(string app_key)
        {
            RestRequest request = new RestRequest(); // { Method = Method.GET };
            request.AddParameter("app_key", app_key);
            request.AddParameter("id", VENUE_ID);

            XmlDocument responseXml = ResponseUtilities.GetResponseContent(BASE_URL, app_key, request);
            return responseXml;
        }

        public bool IsValueAnInteger(string value)
        {
            int n;
            bool isInteger = int.TryParse(value, out n);
            return isInteger;
        }

        public bool IsValueABoolean(string value)
        {               
            if (value == "1" || value == "0")
            {
                return true;
            }

            // Now handle the case where it's a "true" or "false" string. TryParse doesn't recognize 0/1.
            bool n;
            bool isTextBool = bool.TryParse(value, out n);
            return isTextBool;
        }

        public bool IsValueExpectedDataType(string value, string dataType)
        {
            switch (dataType)
            {
                case "boolean":
                    return IsValueABoolean(value);
                case "integer":
                    return IsValueAnInteger(value);
                case "string":
                    return true; // everything is a string
                default:
                    return false;
            }
        }

        static object[] TagExistCases =
        {
            new object[] {"venue"},
            new object[] {"venue/url"},
            new object[] {"venue/name"}
        };

        static object[] TagDataTypeCases =
        {
            new object[] {"venue/url", "string"},
            new object[] {"venue/name", "string"},
            new object[] {"venue/venue_display", "boolean"}
        };
    }
}


