﻿// Written by David Prescott
// Last modified on 2017-09-18
// Copyright (c) 2017, David Prescott

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RestSharp;
using System.Xml;

namespace EventfulAPITester.Events
{
    [TestFixture]
    public class LocataionSearchTests
    {
        string VALID_APPKEY; 

        string BASE_URL;

        [OneTimeSetUp]
        public void PrepareBeforeAnyTests()
        {
            VALID_APPKEY = EventfulAPITester.Properties.Settings.Default.VALID_APP_KEY;
            BASE_URL = @"http://api.eventful.com/rest/events/search";
        }

        #region Location Coords with Radius
        [Test]
        public void Events_Location_ByDistance_NoMatches()
        {
            string coords = "30.986455, -38.157563"; // Middle of the Atlantic.
            string distance = "100";
            string unit = "km";
            XmlDocument responseDoc = GetResponseFromBasicRequest(coords, distance, unit);
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(responseDoc), Is.False);
            Assert.That(ResponseUtilities.GetTagValue(responseDoc, "search/total_items"), Is.EqualTo("0"));
        }

        [Test]
        public void Events_Location_ByDistance_NarrowRadius_SomeMatches()
        {
            string coords = "49.281043, -123.098280"; // This is the location of the Rickshaw Theatre
            string distance = "10";
            string unit = "m";
            XmlDocument responseDoc = GetResponseFromBasicRequest(coords, distance, unit);
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(responseDoc), Is.False);
            Assert.That(ResponseUtilities.GetTagValue(responseDoc, "search/total_items"), Is.GreaterThan("0"));
        }
        #endregion Location Coords with Radius


        public XmlDocument GetResponseFromBasicRequest(string coords, string distance, string unit)
        {
            RestRequest request = new RestRequest();
            request.AddParameter("app_key", VALID_APPKEY);
            request.AddParameter("location", coords);
            request.AddParameter("units", unit);
            request.AddParameter("within", distance);

            XmlDocument responseXml = ResponseUtilities.GetResponseContent(BASE_URL, VALID_APPKEY, request);
            return responseXml;
        }
    }
}
