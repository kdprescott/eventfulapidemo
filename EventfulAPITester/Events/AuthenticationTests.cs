﻿// Written by David Prescott
// Last modified on 2017-09-18
// Copyright (c) 2017, David Prescott

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RestSharp;
using System.Xml;

namespace EventfulAPITester.Events
{
    [TestFixture]
    public class AuthenticationTests
    {
        string VALID_APPKEY;
        string VALID_USERNAME;
        string VALID_PASSWORD;
        string INVALID_APPKEY;

        XmlDocument ValidAppKeyAnonymousUser;
        XmlDocument InvalidAppKeyAnonymousUser;

        string BASE_URL;

        [OneTimeSetUp]
        public void PrepareBeforeAnyTests()
        {
            VALID_APPKEY = EventfulAPITester.Properties.Settings.Default.VALID_APP_KEY;
            INVALID_APPKEY = VALID_APPKEY + "foo";
            BASE_URL = @"http://api.eventful.com/rest/events/search";
           
            // Do the actual Requests
            ValidAppKeyAnonymousUser = GetResponseFromBasicRequest(VALID_APPKEY);
            InvalidAppKeyAnonymousUser = GetResponseFromBasicRequest(INVALID_APPKEY);
        }

        #region Valid App_Key with no User or Password provided
        [Test]
        public void Events_Authentication_VerifyThat_Valid_AppKey_NoOAuth_Produces_NoError()
        {
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(ValidAppKeyAnonymousUser), Is.False);
        }
        #endregion Valid App_Key with no User or Password provided

        #region Invalid App_Key with no User or Password provided
        [Test]
        public void Events_Authentication_VerifyThat_Invalid_AppKey_NoOAuth_Produces_Error()
        {
            Assert.That(ResponseUtilities.WasThereAnErrorInResponse(InvalidAppKeyAnonymousUser), Is.True);
        }

        [Test]
        public void Events_Authentication_VerifyThat_Invalid_AppKey_NoOAuth_Produces_ErrorString()
        {
            Assert.That(ResponseUtilities.GetErrorString(InvalidAppKeyAnonymousUser), Is.EqualTo("Authentication Error"));
        }

        [Test]
        public void Events_Authentication_VerifyThat_Invalid_AppKey_NoOAuth_Produces_ErrorDescription()
        {
            Assert.That(ResponseUtilities.GetErrorDescription(InvalidAppKeyAnonymousUser).Contains("A valid application key is required"), Is.True);
        }
        #endregion Invalid App_Key with no User or Password provided


        public XmlDocument GetResponseFromBasicRequest(string app_key)
        {
            RestRequest request = new RestRequest(); // { Method = Method.GET };
            request.AddParameter("app_key", app_key);
            request.AddParameter("location", "Vancouver");

            XmlDocument responseXml = ResponseUtilities.GetResponseContent(BASE_URL, app_key, request);
            return responseXml;
        }
    }
}
