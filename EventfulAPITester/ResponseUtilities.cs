﻿// Written by David Prescott
// Last modified on 2017-09-18
// Copyright (c) 2017, David Prescott

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
//using System.Xml.Linq;
using RestSharp;

namespace EventfulAPITester
{
    public static class ResponseUtilities
    {
        public static bool IsTagInXML(XmlDocument xmlDoc, string tagName)
        {
            XmlNode node = xmlDoc.SelectSingleNode(tagName);
            return (node != null);
        }

        public static string GetTagValue(XmlDocument xmlDoc, string tagName)
        {
            XmlNode node = xmlDoc.SelectSingleNode(tagName);            
            string value = node.InnerText;
            return value;
        }

        public static string GetErrorString(XmlDocument xmlDoc)
        {
            XmlNode node = xmlDoc.SelectSingleNode("error");
            XmlNode attrVal = node.Attributes.GetNamedItem("string"); // Iffy
            return attrVal.Value;
        }

        public static string GetErrorDescription(XmlDocument xmlDoc)
        {
            XmlNode node = xmlDoc.SelectSingleNode("error//description");
            return node.InnerText;
        }

        public static bool WasThereAnErrorInResponse(XmlDocument xmlDoc)
        {
            return IsTagInXML(xmlDoc, "error");
        }

        public static XmlDocument GetResponseContent(string baseUrl, string app_key, RestRequest request)
        {
            var client = new RestClient(baseUrl);
            var response = client.Execute(request);
            XmlDocument responseXml = new XmlDocument();
            responseXml.LoadXml(client.Execute(request).Content);
            return responseXml;
        }

    }
}
